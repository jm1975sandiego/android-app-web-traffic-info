### What is this repository for? ###

* Android application for monitoring website traffic directed from a website traffic provider (such as [ VisitorMaker.Com ](https://www.visitormaker.com/) or others)
* Version 1.1

### Notes ###

* Android platform offers this information through TrafficStats class Configuration
* The user needs basic knowledge of Android development
* RefreshTrafficStats method reads the traffic information and updates the TextView